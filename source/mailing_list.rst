Mailing list
============

- backports@vger.kernel.org `(archives) <https://lore.kernel.org/backports>`__
- `Subscribing <https://subspace.kernel.org/subscribing.html>`__

Topics
------

Contact the backports@vger.kernel.org mailing list with:

- patches
- bug reports
- questions regarding the backports project
