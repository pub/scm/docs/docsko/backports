Backports
=========

The *Backports Project* enables old kernels to run the latest drivers.

"*Backporting*" is the process of making new software run on something
old. A version of something new that's been modified to run on something
old is called a "*backport*".

The Backports Project develops tools to automate the backporting process
for Linux drivers. These tools form the *backports* suite.

History
-------

The Backports Project started in 2007 as *compat-wireless*. It was
renamed to *compat-drivers* as the project's scope broadened beyond just
wireless network drivers. Nowadays, the project is known simply as
*backports*.

As of the 3.10-based release, over 830 device drivers had been
backported.

Recent versions of backports support mainline kernels back to version
3.0. The older backports-3.14 supports all kernel versions back to
version 2.6.26.

.. toctree::
    :maxdepth: 1

    Package releases: (download) <releases>
    Documentation <documentation>
    Reporting bugs & security vulnerabilities <bugs>
    code
    config-brainstorming
    Contributing <documentation/backports/hacking>
    Linux kernel backports license <license>
    Mailing list <mailing_list>
    IRC <irc>
    Social Media <social_media>
    Meetings <meetings>

Resources
---------

- Daily snapshots: `(linux-next)
  <http://git.kernel.org/cgit/linux/kernel/git/next/linux-next.git>`__
  `(linux-stable)
  <http://git.kernel.org/cgit/linux/kernel/git/stable/linux-stable.git>`__

- `git repository
  <https://git.kernel.org/cgit/linux/kernel/git/backports/backports.git>`__

- `Bugzilla bug tracker:
  <https://bugzilla.kernel.org/enter_bug.cgi?product=Backports%20project>`__
  :doc:`(notes) <bugs>`

- `Increasing Automation in the Backporting of Linux Drivers Using
  Coccinelle - Luis R. Rodriguez, Julia Lawall
  <http://coccinelle.lip6.fr/papers/backport_edcc15.pdf>`__ (12 pages)

- `Automatically Backporting the Linux Kernel - Luis Rodriguez
  <https://www.youtube.com/watch?v=ZXATzae7eng>`__ (54 minutes), SUSE
  Labs Conference, České Budějovice, October 2014

- `An Introduction to Coccinelle Bug Finding and Code Evolution for the
  Linux Kernel - Julia Lawall
  <https://www.youtube.com/watch?v=buZrNd6XkEw>`__ (1 hour 58 minutes),
  SUSE Labs Conference, České Budějovice, October 2014

- `An Update on the Linux Backports Project - Luis R. Rodriguez,
  Qualcomm Atheros <https://www.youtube.com/watch?v=lvFXQ6zyHCg>`__ (43
  minutes), LinuxCon and CloudOpen Conference, New Orleans, October 2013
