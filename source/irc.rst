IRC
===

Join us::

    irc.freenode.net #kernel-backports

Do not ask for permission to ask a question in IRC, just ask it with all
the information you have and wait for someone answering.
