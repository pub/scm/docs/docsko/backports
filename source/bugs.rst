Bugs
====

Report **any bugs** you **think** you **may** have found because you are
working with the **latest and greatest drivers**! If your bug is
backport compatibility-related then we should still try to fix it within
the compatibility layer. If you are not sure you can let us help you.
**To date we have only have identified 2 backport related bugs**, all
other reported bugs were **real bugs** in **actual upstream** code!

We are working on a bugzilla entry for compat / compat-drivers, until
then please report all bugs you think you my have found to the
:doc:`backports mailing list <mailing_list>` and if you know what subsystem
you think the bug belongs to Cc: the respective mailing list. Remember
that chances are **high** that the bug is not a backport bug but instead
a real upstream bug that **must** be fixed.

What mailing lists to report bugs to
------------------------------------

Always use: **backports@vger.kernel.org** and then use one of the
following mailing lists depending on the subsystem from where your
driver belongs:

- bluetooth: linux-bluetooth@vger.kernel.org
- wireless: linux-wireless@vger.kernel.org
- ethernet: netdev@vger.kernel.org

Reporting security vulnerabilities
----------------------------------

If you have a security vulnerabilities issue to report and you know it
is backports related you can report this directly to the maintainers:

- hauke@hauke-m.de
- mcgrof@kernel.org
- johannes@sipsolutions.net

The report will be handled in private, once the issue is fixed and
propagated to users, the security fix will be disclosed and documented.
As of date we have had no security vulnerabilities issues reported.
Until then this page can be used to track updates on vulnerabilities
related to Linux backports. The attack surface to Linux backports
consists about 1-2% of code, this varies depending on what kernel you
are on. The older kernel you are on the higher the security risk.
Security issues on Linux should affect users of Linux backports if the
code is carried over into backports, fixes for that are addressed
through new release of backports with the corresponding upstream fixes.
Security fixes for Linux belong upstream on Linux, not on Linux
backports. To learn how to report Linux kernel security issues refer to
`SecurityBugs documentation
<https://www.kernel.org/doc/Documentation/SecurityBugs>`__.

Bugzilla
--------

The Linux kernel bugzilla has an entry for backport bugs.

Reporting new backport bugs
~~~~~~~~~~~~~~~~~~~~~~~~~~~

To record a new bug:

#. access the `Backports project <https://bugzilla.kernel.org/enter_bug.cgi?product=Backports%20project>`__ section of Bugzilla
#. select the *backports* component (it is the only component available)

Viewing bugs by status
----------------------

-  [https://bugzilla.kernel.org/buglist.cgi?query_format=specific&order=relevance+desc&bug_status=__open__&product=Backports+project&content=. View open bugs]
-  [https://bugzilla.kernel.org/buglist.cgi?query_format=specific&order=relevance+desc&bug_status=__closed__&product=Backports+project&content=. View closed bugs]
