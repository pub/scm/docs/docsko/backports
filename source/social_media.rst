Social media
============

Linux backports social media
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Apart from making announcements on the mailing lists you can also
subscribe to the following social media outlets to get updates:

- `@LinuxBackports on Twitter <https://twitter.com/LinuxBackports>`__
- `@LinuxBackports on identi.ca <http://identi.ca/linuxbackports>`__
