Documentation
=============

.. toctree::
   :maxdepth: 1

   documentation/packaging
   documentation/integration
   documentation/compat
   documentation/compat-drivers
   documentation/compat-drivers/hacking
   documentation/compat-drivers/additional-patches
   documentation/backports/hacking
   documentation/backports/hacking/todo

The Backports Project aims to backport current Linux **upstream** device
drivers for use with older kernels. The objective (1) is to provide a
central mechanism for backporting the device drivers of *any* subsystem,
thereby enabling (2) *both* users **and** developers to always focus on
upstream Linux kernel development.

The project shall **never** include proprietary drivers, and is designed
to disallow its use with proprietary drivers.

Every backports release has been test compiled *for usage* against all
supported kernels. The oldest release is (currently) 3.0.

Linux kernel releases can become deprecated. You are encouraged to use
supported stable kernels as listed on `kernel.org
<https://kernel.org>`__.

Workflows
---------

Backports provides users with a choice of two workflows:

#. **kernel integration mode** (:doc:`documentation/integration`)

   - future kernel source tree and older kernel source tree must be
     present on the same machine at the same time
   - backports suite **integrates** the subsystems/drivers of the future
     kernel directly into the older kernel

#. **package releases mode** (:doc:`documentation/packaging`)

   - future kernel source tree and older kernel source tree do not need
     to be present on the same machine at the same time
   - on machine hosting future kernel source tree, backport package is
     generated
   - on machine hosting older kernel, backport package is built
     out-of-tree against older kernel
   - backport package is loosely akin to a patch file

Backported Subsystems
---------------------

Device drivers are available for the following subsystems:

- Ethernet
- Wireless
- Bluetooth
- NFC
- ieee802154
- Media
- Regulator

Backported Drivers
------------------

Whether or not a device driver is available from a subsytem will depend
on whether or not a developer decided to backport it **and** if the
device driver is backported down to the kernel you are on. If you see
the driver on **make menuconfig** it means you can use it. An
alternative is to look at the git tree `dependencies
<https://git.kernel.org/cgit/linux/kernel/git/mcgrof/backports.git/tree/dependencies>`__
file. Note that the `dependencies
<https://git.kernel.org/cgit/linux/kernel/git/mcgrof/backports.git/tree/dependencies>`__
**does not** exist on a final release, it only exists on the development
git tree and the one linked here is the one on the master branch -- you
should look at the `release branches
<https://git.kernel.org/cgit/linux/kernel/git/mcgrof/backports.git/refs/>`__
for their respective dependencies file if using an older release.
Someone is welcome to come up with a fancy page that provides the device
driver <--> kernel dependency map page. If a device driver is available
on **make menuconfig** but is not listed on the `dependencies
<https://git.kernel.org/cgit/linux/kernel/git/mcgrof/backports.git/tree/dependencies>`__
file it means it is available for usage on all supported kernel.

Users should just install what they *know* they need, if not sure don't
enable a driver. Typically Linux distributions would use the backports
project and build modules for you and you'd have a backports package
available for your distribution.
