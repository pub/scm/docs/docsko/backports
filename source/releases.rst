Releases
========

Latest releases
===============

All these releases should work against the same kernel major version and
all older versions till 3.10 for backports versions till 5.10. Later
versions of backprots should work with kernel versions down to version
4.4.

-  `backports-6.1.97-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v6.1.97/backports-6.1.97-1.tar.xz>`__

   -  SHA256: 8e9ae2d02f373252dd61f5c6a81c88eec67ca773464d9ef3d844752dc6775540

-  `backports-5.15.153-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.15.153/backports-5.15.153-1.tar.xz>`__

   -  SHA256: eaa24df968c79385c57707068a209fb1ea43271b573f24885805ae96a58ee3a8

-  `backports-5.14.13-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.14.13/backports-5.14.13-1.tar.xz>`__

   -  SHA256: 042aef20caf17ef649502d5f2e744a7676abe7faed18de83c96f37bc029635fe

-  `backports-5.13.19-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.13.19/backports-5.13.19-1.tar.xz>`__

   -  SHA256: 1eb761c1664d59a0a2d52847f240c9d02fc2e56fb27d862d2949bfb275187eef

-  `backports-5.12.19-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.12.19/backports-5.12.19-1.tar.xz>`__

   -  SHA256: 4233002bcf26237783cd517d93c27807e534234cef64def7e550c5f06b779d18

-  `backports-5.11.22-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.11.22/backports-5.11.22-1.tar.xz>`__

   -  SHA256: 35c23dd182711a3a6f671a673c2e0ace2ffdd25bbd1fb917e428d04924141299

-  `backports-5.10.157-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.10.157/backports-5.10.157-1.tar.xz>`__

   -  SHA256: 1ce937c49f2b39be00768fba83e214aad6612d469c92ccd06dc17b14e6cf3a64

-  `backports-5.9.12-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.9.12/backports-5.9.12-1.tar.xz>`__

   -  SHA256: 8cf3f23152a787eeec2df1d7b38a559b0d89bdc823d9ac99175a4eb76d2b619e

-  `backports-5.8.18-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.8.18/backports-5.8.18-1.tar.xz>`__

   -  SHA256: f04a8172423c6a945fc7d9844b04f33fda9ae574e552f8f18ee3bdfcfb494563

-  `backports-5.7.5-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.7.5/backports-5.7.5-1.tar.xz>`__

   -  SHA256: 200312d46eb6a94676f611af6baebbffc5cbf3b7dd75e72a69c767704c37b571

-  `backports-5.6.8-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.6.8/backports-5.6.8-1.tar.xz>`__

   -  SHA256: 547c5e17b9e23dd23cdf4d617a7550b80869e02114a7d404911c5ae928ae1da5

-  `backports-5.5.19-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.5.19/backports-5.5.19-1.tar.xz>`__

   -  SHA256: 9dd9153df6082eaa079144193a3fab79d200942e1a2a1a80e032c9667b7b92a6

-  `backports-5.4.56-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.4.56/backports-5.4.56-1.tar.xz>`__

   -  SHA256: 6030c2427e04f976cf53330087d0fdbb60ead7e1fecbd85e6f8e97ec0ed9a2eb

-  `backports-5.3.6-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.3.6/backports-5.3.6-1.tar.xz>`__

   -  SHA256: 16ded706945999543a73e2349d36b8003eeb2b097970ea1ad80344b9f56393a3

-  `backports-5.2.8-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.2.8/backports-5.2.8-1.tar.xz>`__

   -  SHA256: d1ac22a9b7536f730a992292fb29c70355ffc42ea9f58610010ea196dc69b2e3

-  `backports-5.1.16-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.1.16/backports-5.1.16-1.tar.xz>`__

   -  SHA256: b5adc5d458734b9231e81bcf03af2fb1bf2e289a87f1551a4f02bdf3ba053fb8

-  `backports-5.0.5-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v5.0.5/backports-5.0.5-1.tar.xz>`__

   -  SHA256: 9fe6eeaf2043a6eadbc5a13277b0ac62905451af55d7afaa6ac2d91a3aa8c298

-  `backports-4.20.17-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v4.20.17/backports-4.20.17-1.tar.xz>`__

   -  SHA256: c7efc87babef35c08251a04cb26ffd683026d1365f5bbf8226c9e22462c41592

-  `backports-4.19.221-1.tar.xz <https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/v4.19.221/backports-4.19.221-1.tar.xz>`__

   -  SHA256: 343f54b21ddda4bc79c0457a7fa88356d430b802f86194abc20fe09c12559b05

All releases: https://cdn.kernel.org/pub/linux/kernel/projects/backports/stable/
