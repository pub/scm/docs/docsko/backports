Documentation/backports/hacking/todo
====================================

The Linux kernel backports TODO list.

make localmodconfig support
---------------------------

The Linux kernel has *make localmodconfig* support, random users
shouldn't be expected to know what modules they need, so in order to
allow users to get only the drivers they need built we'd need something
similar to *make localmodconfig*. There are a few ways to do this. The
best way would be to get upstream a mapping of a kernel object of a
device to list its respective Kconfig. We'd also need a map of Kconfig
changes over time as the kernel advances so that if a user is on an
older system their old kobject Kconfig could be mapped to a new one.
This should require changes upstream.

Other simpler solutions are to rely on 'lspci -k' output and map this to
respective device drivers but this solution would be PCI bus specific.
We should review how the upstream *make localmodconfig* currently works
and improve upon that to enable support for it on backports.

Who's working on this?
~~~~~~~~~~~~~~~~~~~~~~

No one.

Module namespaces
-----------------

Module namespace can consist of either adding prefixes to existing
symbols or modifying the Linux kernel module data structure so that upon
request for a symbol it's own namespace can be inspected for its
requested namespace. The Linux kernel backports project embraces
prefixing backported versions of symbols by prefixing backported
versions of symbols with *backports\_*. A macro LINUX_BACKPORT() was
added to help with this. For example::

    #define crc8 LINUX_BACKPORT(crc8)

Andi Kleen introduced the modular structure modification version of
`module namespaces
<http://thread.gmane.org/gmane.linux.network/78674>`__ back in 2007 but
`Rusty Russell nacked these patches
<http://article.gmane.org/gmane.linux.kernel/606885>`__. Because of this
we're left with remapping modules to their own namespaces without
modifying the in-kernel module structure. It **may** be possible to
somehow provide module namespaces without requiring modifications to the
in-kernel module structure, but such mechanisms would end up being
hacks. We need to evaluate if we can figure a clever way to support this
that is not so error prone or if we want to respinning Andi's patches
with the intent to help support kernel backporting.

Original motivation for module namespaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here's a brain dump from Andi about the original motivation behind
module namespaces:

The original intent was to provide a cleaner export symbol API.

There are roughly two classes of exported symbols:

- Generally usable, well designed, interfaces intended to be used by
  multiple modules (e.g. functions for drivers to register themselves or
  library functions)
- Special purpose exports that are only really for a single module, but
  are not intended as a general interface. These interfaces are usually
  not really clean and reusable.

The idea was to mark the later special purpose exports with the name of
the module that is supposed to them. They wouldn't be available to
everybody else and wouldn't become part of the general kernel module
API.

Motivations
~~~~~~~~~~~

At times Linux distributions may choose, for whatever support-nightmare
reasons, to support two versions of a subsystem module and have two
separate device drivers use a different version of that subsystem. The
reason this is ill-advised is we stand to gain by having development
done on the upstream and latest versions of each subsystem and drivers;
that backports framework allows you to get that. An example helps to
clarify here. Suppose a distribution wants to ship with an old version
of mac80211 and have the iwlwifi driver use it, but for ath9k they'd
want to ship the newer version of mac80211 and new version of ath9k.
This cannot be supported unless module namespaces are created.

Another more reasonable motivation is to avoid regressions with features
/ symbols being updated only for backported drivers. Non-backported
drivers will use the non-backported version of the routine. This should
avoid regressions with non-backported drivers using these routines.

Who's working on this?
~~~~~~~~~~~~~~~~~~~~~~

No one.

DKMS support
------------

Someone seems to want add support for `DKMS
<https://en.wikipedia.org/wiki/Dynamic_Kernel_Module_Support>`__. Who
was that? What benefits does this bring? Isn't DKMS for proprietary
modules? If so we don't want this support. We don't want proprietary
drivers using the Linux kernel backports framework.

DKMS is for proprietary modules only because all proprietary modules are
out-of-tree, and DKMS is for all out-of-tree modules, whether they are
proprietary or not. Since Linux kernel backports are out-of-tree, DKMS
brings the benefit of automagically keeping existing backports active
when a new kernel is installed, rather than going back to the in-tree
modules with every new kernel.

Who's working on this?
~~~~~~~~~~~~~~~~~~~~~~

No one
