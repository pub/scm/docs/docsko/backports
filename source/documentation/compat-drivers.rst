Documentation/compat-drivers
============================

compat-drivers is the framework that **pulls** code from Linux kernel
releases and strives to backport them *automatically* for usage on older
Linux kernel releases. compat-drivers allows us to make releases of code
from newer Linux kernel releases and get the latest drivers to users
without having to recompile their entire kernel.

Supported Linux kernel releases
-------------------------------

The `kernel.org <http://kernel.org/>`__ archive hosts **all** kernel
releases. As the Linux kernel development moves on older kernels get
**deprecated** and a few kernels remain supported. It is imperative that
end users / system integrators / Linux distributions upgrade to newer
**supported** kernels regularly otherwise they may end up being stuck on
**unsupported** kernels. The compat-drivers project aims at **always**
providing support of newer drivers down to *at least* **all supported
Linux kernel releases** as listed on `kernel.org
<http://kernel.org/>`__. As it is though the project currently takes
backporting further and it does support a large amount of older
unsupported and deprecated Linux kernel releases. As it is right now all
21 older kernels are supported: 2.6.24 - 3.6.

Release types
-------------

There are two release types, linux-stable and linux-next releases, for
details refer to the :doc:`releases page <../releases>`. There are also
modifications to releases and an extra set of flags are appended to
releases depending on the series of types of patches applied to them.
For details on these different type of patches that can be applied onto
a release refer to the :doc:`legend of additional patches
<compat-drivers/additional-patches>` section.

Backported subsystems
---------------------

There are three subsystems currently backported through this framework.

- Ethernet
- Wireless
- Bluetooth

The video drivers are currently on their way to being integrated.

Subsystem requirements
~~~~~~~~~~~~~~~~~~~~~~

In order to compile and install modules from the compat-drivers releases
you will need to have your kernel headers installed on your system. As
you get new drivers though you may need to install newer firmware as
well so be sure to always have in synch your **/lib/firmware/** directly
with the latest firmware from the `linux-firmware.git tree
<http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=summary>`__.

Each backported subsystem may also take advantage of newer userspace
APIs and as such you should upgrade each subsystem's recommended
userspace as documented here.

Userspace upgrade for wireless
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We recommend these the following 802.11 wireless userspace applications
should be upgraded:

- `wireless-regdb <http://wireless.kernel.org/en/developers/Regulatory#The_regulatory_database>`__
- `CRDA <http://wireless.kernel.org/en/developers/Regulatory/CRDA>`__
- `iw <http://wireless.kernel.org/en/users/Documentation/iw>`__
- `rfkill <http://wireless.kernel.org/en/users/Documentation/rfkill>`__
- `linux-firmware <http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=summary>`__
- `wpa_supplicant <http://wireless.kernel.org/en/users/Documentation/wpa_supplicant>`__
- `hostapd <http://wireless.kernel.org/en/users/Documentation/hostapd>`__

Userspace upgrade for bluetooth
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Someone should edit this and update this.

Userspace upgrade for ethernet
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Someone should edit this and update this.

Building and installing
-----------------------

This section covers how to use build and install compat-drivers.

Selecting your driver
~~~~~~~~~~~~~~~~~~~~~

compat-drivers allows you to selectively build only one driver instead
of building all backported drivers.

Extracting compat-drivers
-------------------------

Extract the content of the package::

   tar xvf compat-drivers-$(date -I).tar.bz2

If you know what driver you want you can use driver-select::

   ./scripts/driver-select

Selecting your driver can reduce the amount of time to compile
compat-drivers. It may also enable kernel compatibility down to older
kernels. If you want to restore compat-drivers to enable all drivers
again you can use::

   ./scripts/driver-select restore

If you do not see your driver in the supported list it means care has
not been taken to ensure it works with driver-select and you must just
build compat-drivers as a whole.

Building and installing
-----------------------

This section documents how to build and install compat-drivers.

Building compat-drivers
~~~~~~~~~~~~~~~~~~~~~~~

Build the latest backported Linux subsystems::

   cd compat-wireless-$(date -I)
   make

Installing compat-drivers
~~~~~~~~~~~~~~~~~~~~~~~~~

We use the updates/ directory so your distribution's drivers are left
intact. By default depmod will search for this directory first, but if
you find that it does not then check that your depmod.conf file lists
the search order correctly with *updates* appearing first (before
*built-in*). the package should automatically detect this for you and
deploy an update for you.

::

   sudo make install

Uninstalling compat-drivers
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This nukes our changes to updates/ so you can go back to using your
distribution's supported drivers.

::

   sudo make uninstall

Unloading your old distribution drivers
---------------------------------------

Since you might be replacing your old subsystem drivers you should first
try to unload all existing related drivers. We provide a mechanism to
unload all drivers::

   sudo make unload

If unsure just reboot.

Loading compat-drivers
~~~~~~~~~~~~~~~~~~~~~~

Before loading modules you must first unload your old subsystem modules.
Read above how to do this. If you know what module you need you can
simply load the module using modprobe. If you are not sure you can just
reboot the box.

Building for external kernels
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have a kernel you do not have installed but yet want to build the
compat-drivers for it you can use this syntax::

   make KLIB=/home/mcgrof/kernels/linux-2.6.27.22 \
      KLIB_BUILD=/home/mcgrof/kernels/linux-2.6.27.22

Additional patches
------------------

Read the :doc:`additional patches <compat-drivers/additional-patches>`
section for details of how **emergency** or **critical** fixes are dealt
with while still **prioritizing** Linux upstream development.

Bugs
----

Please see the :doc:`reporting bugs section <../bugs>`. This
cannot be stressed enough: **to date we have only have identified 2
backport related bugs**, all other reported bugs **were real bugs in
actual upstream code** so be sure to report any bugs found **ASAP**!

License
-------

This work is a subset of the Linux kernel as such we keep the kernel's
Copyright practice. Some files have their own copyright and in those
cases the license is mentioned in the file. All additional work made to
building this package is licensed under the GPLv2.

Hacking compat-wireless
-----------------------

If you'd like to hack on compat-drivers or make your own releases refer
to the :doc:`hacking compat-drivers page <compat-drivers/hacking>`.

