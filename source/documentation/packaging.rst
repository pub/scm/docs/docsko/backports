Documentation/packaging
=======================

The backports project makes release of two types of **packages** which
provides device drivers from future versions of Linux usable on older
Linux kernel releases.

Backports package release types
-------------------------------

Both daily snapshots based on `linux-next
<http://git.kernel.org/cgit/linux/kernel/git/next/linux-next.git>`__,
and stable releases based `Linux's stable releases
<http://git.kernel.org/cgit/linux/kernel/git/stable/linux-stable.git>`__
are provided. Always use the latest *stable* available release unless
you need a feature / fix only currently available on the linux-next
based release. A backports-3.x release means device drivers from the
Linux v3.x release have been taken, backported and made available for
you to use on any kernel version prior to the release version.

Usage guide
-----------

Building backports follows the same build mechanism as building the
Linux kernel::

   # as a user
   make menuconfig
   make -j4
   # as root
   make install
   # reboot and enjoy

Its understood users may not know how to configure the backports
package, just like they may not know how to configure the Linux kernel,
so a short cut is provided with default configuration files that can be
used to only build their drivers / subsystems of interest. You can also
just query the regular help menu::

   make help
   make defconfig-help

If you use this option just use the 'make defconf-option' in replacement
for *make menuconfig* above. For example to compile all wifi drivers::

   # as a user
   make defconfig-wifi
   make -j4
   # as root
   make install

Note that there are only default configuration files written for a few
drivers while the project actually backports a lot of device drivers,
the reason we have default configuration files for a few drivers is
simply because developer have provided a default config options for
them. What we really need is a 'make localmodconfig' support but that
will take a while given that it involves mapping older kernel configs to
newer kernel configs (which likely would be welcomed upstream as well).

Cross compiling
---------------

To cross compile::

    set -a
    CROSS_COMPILE=${CROSS_COMPILE}
    ARCH=${TARGET_CPU}
    KLIB_BUILD=${DEV_PATH}/${LINUX_DIR}
    KLIB=${TARGET_ROOT_ON_HOST}
    set +a
    make oldconfig  # menuconfig worked here too
    make
    make install

The 'make install' target isn't currently sane for cross-builds due to
the bacport_firmware_install script not respecting prefixes. For now you
can comment out that script few others like initrd updates, from being
run out of the Makefiles.

Cross compile with Freescale's LTIB
-----------------------------------

To get backports happy in LTIB, use UNSPOOF/SPOOF_PATH to switch between
host and cross environment. Example Build section in backports.spec::

     %Build
     export PATH=$UNSPOOF_PATH
     
     make menuconfig prefix=%{_prefix} \
       CROSS_COMPILE=${TOOLCHAIN_PATH}/bin/${TOOLCHAIN_PREFIX} \
       ARCH=$LINTARCH KLIB=${TOP}/rootfs/lib/modules/%{kversion} \
       KLIB_BUILD=${TOP}/rpm/BUILD/linux
     
     export PATH=$SPOOF_PATH
     
     make prefix=%{_prefix} \
       CROSS_COMPILE=${TOOLCHAIN_PATH}/bin/${TOOLCHAIN_PREFIX} \
       ARCH=$LINTARCH KLIB=${TOP}/rootfs/lib/modules/%{kversion} \
       KLIB_BUILD=${TOP}/rpm/BUILD/linux
