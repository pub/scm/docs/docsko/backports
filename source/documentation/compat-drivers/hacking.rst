Documentation/compat-drivers/hacking
====================================

This is now deprecated use :doc:`hacking on backports <../backports/hacking>`.

This section deals with development details of compat / compat-drivers
and the other trees it uses. If you want to make your own compat-drivers
tarballs, or if you see something busted with compat-drivers or just
want to add something new or an enhancement this is the guide for you.

Git trees you will need
-----------------------

compat-drivers backports a few subsystems down to older kernels. To be
able to synchronize backporting the latest and greatest the
`linux-next.git
<http://git.kernel.org/?p=linux/kernel/git/next/linux-next.git;a=summary>`__
tree is used as its main source for kernel updates. General Linux kernel
compatibility is addressed through a general kernel compatibility tree,
`compat.git <https://github.com/mcgrof/compat>`__. compat-drivers then
has its own tree for specific subystem target backports. You will then
need to checkout a few trees to start hacking on compat-drivers, you can
use the :doc:`instructions on getting all required code <../../code>`
to get all trees but below we provide the list of the git trees used.

::

    git://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git
    git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
    git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
    git://github.com/mcgrof/compat.git
    git://github.com/mcgrof/compat-drivers.git

Linux next
----------

The `linux-next.git
<http://git.kernel.org/?p=linux/kernel/git/next/linux-next.git;a=summary>`__
tree brings all subsystems being worked on for the **next kernel
release** into one tree. So if the current rc kernel is v3.6-rc1, this
means linux-next will have what people today are working on for the 3.7
kernel release.

backports components
--------------------

Go read the :doc:`backports documentation <../../documentation>` for a list of
backport components and ensure you understand what each one is supposed
to do. Below is a quick description though for the big picture.

compat.git
~~~~~~~~~~

The compat git tree is a general kernel compatibility layer which can be
shared amongst different compatibility projects, or drivers.
compat-drivers is just one of the kernel compatibility projects using
compat.git. compat.git builds a general compatibility module, compat,
and any additional modules to let you get new general kernel updates
from future kernels on your old kernels.

compat-drivers.git
~~~~~~~~~~~~~~~~~~

Anything that is not general kernel compatibility but instead specific
to a subsystem goes into compat-drivers.git. After you've cloned all
three trees, linux-next.git, compat.git and compat-wireless.git you need
to change into the compat-drivers directory and tell compat-drivers
where you linux-next and compat.git trees are. You do this with
environment variables GIT_TREE and GIT_COMPAT_TREE. You can do for
example::

   export GIT_TREE=/home/user/linux-next/
   export GIT_COMPAT_TREE=/home/users/compat/

Then you can then update your local sources based on these
linux-next.git and compat.git trees::

   scripts/admin-clean.sh   - Cleans the compat-drivers tree
   scripts/admin-update.sh  - Updates compat-drivers with your git tree
   scripts/admin-refresh.sh - Does the above two

Adding new drivers
------------------

Most new drivers are enabled for compilation. If see a driver you would
like enabled try adding it into the mix, test them and if they work
enable them and send the respective patches.

Generating stable releases
--------------------------

You can make stable releases yourself by checking out the specific
branch for the target stable kernel release you want on each git tree:

- `linux-stable.git <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=summary>`__
- `compat-drivers.git <https://github.com/mcgrof/compat-drivers>`__
- `compat.git <https://github.com/mcgrof/compat>`__

You will also want an updated `linux-next.git
<http://git.kernel.org/?p=linux/kernel/git/next/linux-next.git;a=summary>`__,
you want to ensure that the tag for the target kernel you want is
present if you want to cherry pick out stable patches from it. So for
example if v3.5.1 was released you want to ensure the `linux-next.git
<http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=summary>`__
tree you have is updated with the v3.5.1 tag. The `linux-next.git
<http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=summary>`__
tree however is **not** carry the extra version stable releases however
though so to add these tags you can add the both the linux.git and
linux-stable.git as remotes to your **linux-next/.git/config** file::

   [remote "next"]
           fetch = +refs/heads/*:refs/remotes/origin/*
           url = /home/mcgrof/linux/.git
   [remote "stable"]
           fetch = +refs/heads/*:refs/remotes/origin/*
           url = /home/mcgrof/linux-stable/.git

Then do on your linux-next directory::

   git fetch next
   git fetch -t next
   git fetch stable
   git fetch -t stable

Now, for example if you wanted to make a v3.5.y stable release, you
would do::

   # On linux-next.git:
   mcgrof@tux ~/linux-next (git::master)$ git fetch
   remote: Counting objects: 43056, done.
   ... etc ...
   mcgrof@tux ~/linux-next (git::master)$ git reset --hard origin
   HEAD is now at 74bdbee Add linux-next specific files for 20110329
   # Now get kernel extraversions on linux-next.git, this allows you
   # to extract stable patches from linux-next.git when the linux-2.6-allstable
   # tree moves to a new extraversion release.
   mcgrof@tux ~/linux-next (git::master)$ git fetch stable
   mcgrof@tux ~/linux-next (git::master)$ git fetch -t stable

   # For compat-drivers.git:
   mcgrof@tux ~/devel/compat-drivers (git::master)$ git checkout -b linux-3.5.y origin/linux-3.5.y
   Branch linux-3.5.y set up to track remote branch linux-3.5.y from origin.
   Switched to a new branch 'linux-3.5.y'
   mcgrof@tux ~/devel/compat-drivers (git::linux-3.5.y)$

   # For compat.git:
   mcgrof@tux ~/compat (git::master)$ git checkout -b linux-3.5.y origin/linux-3.5.y
   Branch linux-3.5.y set up to track remote branch linux-3.5.y from origin.
   Switched to a new branch 'linux-3.5.y'
   mcgrof@tux ~/compat (git::linux-3.5.y)$ 

   # For linux-stable.git:
   mcgrof@tux ~/linux-stable (git::master)$ git checkout -b linux-3.5.y origin/linux-3.5.y
   Branch linux-3.5.y set up to track remote branch linux-3.5.y from origin.
   Switched to a new branch 'linux-3.5.y'

Once you have the appropriate branches checked out, you can use a script
designed to let you make a release:

#. First tell compat-wireless scripts your target GIT_TREE
#. from where you want to suck out code from is the stable tree::

        mcgrof@tux ~/devel/compat-drivers (git::linux-3.5.y)$ export GIT_TREE=/home/mcgrof/linux-stable/

#. Then generate a stable release::

    mcgrof@tux ~/devel/compat-drivers (git::linux-3.5.y)$ ./scripts/gen-stable-release.sh -s -n
    Skipping linux-stable git tree update checks for branch: linux-3.5.y
    On linux-stable: linux-2.3.5.y
    You said to use git tree at: /home/mcgrof/linux-stable for linux-next
    Copying /home/mcgrof/linux-stable/include/linux/ieee80211.h
    Copying /home/mcgrof/linux-stable/include/linux/ieee80211.h
    Copying /home/mcgrof/linux-stable/include/linux/nl80211.h
    Copying /home/mcgrof/linux-stable/include/linux/pci_ids.h
    ... Creating compat-wireless-3.5.1-1-sn.tar.bz2 ...

    Compat-wireles release: compat-wireless-v3.5.1-1-sn Size: 4.3M
    compat-wireless-v3.5.1-1-sn.tar.bz2 sha1sum: b0ca93dbda466b22ed76a8e4792c89931090d7b3 compat-wireless-v3.5.1-1-sn.tar.bz2
    Release: /tmp/staging/compat-wireless/compat-wireless-v3.5.1-1-sn.tar.bz2

Note that if you supplied the -s flag you will want to review the output
for the place where it generates the pending-stable patches. If the
respective target upstream tag was not found on linux-next.git it will
tell you.

If you want to add additional non-upstream patches you can use the crap/
directory and use the -c flag as well. When people review your tarball
they can then find your delta easily. If you submit patches upstream you
can stuff them into linux-next-pending/ and use -p. If your patch is
upstream on linux-next.git you can cherry pick it out and stuff it into
linux-next-cherry-picks/ and use -n. The purpose of all this effort is
to enable customizations but to also allow reviewers to quickly find
deltas and ensure code gets upstream properly. Check each respective
directory README for a review of the directories intent and content.

Sending patches
---------------

compat and compat-drivers contributions follow the contribution model
implemented by the Linux kernel. Patches or pull requests for compat and
compat-drivers must have be signed-offed. If you don't sign off on them
**they will not accepted**. This means adding a line that says
"Signed-off-by: Name " at the end of each commit, indicating that you
wrote the code and have the right to pass it on as an open source patch.
For exact definition of what the Signed-off-by tag is you can read the
definition of the "**Developer's Certificate of Origin 1.1**", which you
can read here:

http://gerrit.googlecode.com/svn/documentation/2.0/user-signedoffby.html

Remember there are three trees. linux-next itself is a conglomeration of
kernel git trees itself, so patches for linux-next.git should be sent to
each respective subsystem for which the patches are targeted for. So for
example for 802.11 you will want to send them to John Linville and cc
linux-wireless, for further guidelines on this see the `Submitting
Patches guidelines for 802.11
<http://wireless.kernel.org/en/developers/Documentation/SubmittingPatches>`__.
As another example, for bluetooth you will want to send them to Gustavo
Padovan and cc the linux-bluetooth mailing list. If your patch touches
on others areas of the kernel refer to the MAINTAINERS file on the
kernel.

For compat.git and compat-drivers.git please send patches against to::

   To: Luis R. Rodriguez <mcgrof@kernel.org>
   CC: backports@vger.kernel.org
   Subject: [PATCH] compat-2.6: fix foo

For patches for compat.git please use a subject like the following::

   Subject: [PATCH] compat: fix foo

For compat-drivers.git please use a subject like the following::

   Subject: [PATCH] compat-drivers: fix foo

Patches are preferred sent with a clear commit log entry, if unfamiliar
with how to send patches please refer to `a git guide
<http://wireless.kernel.org/en/developers/Documentation/git-guide>`__.

TODO
----

- Dialog (make menuconfig) option for this package -- :doc:`../../config-brainstorming`
- Add support for `DKMS <https://en.wikipedia.org/wiki/Dynamic_Kernel_Module_Support>`__
- Add more subsystems
- media (e.g. DVB-T devices)

