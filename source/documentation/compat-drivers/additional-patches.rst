Documentation/compat-drivers/additional-patches
===============================================

This is now deprecated use :doc:`hacking on backports <../backports/hacking>`.

Additional patches to stable releases
-------------------------------------

compat-drivers **prioritizes** pushing software to be **upstreamed**
into *respective* Linux subsystem development trees. The stable
compat-drivers releases are based on vanilla stable kernels and extra
version stable (3.x.y) releases as published through the
`linux-stable.git
<http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=summary>`__
tree. Stable fixes are propagated through the `stable kernel fix
propagation mechanism
<http://wireless.kernel.org/en/users/Documentation/Fix_Propagation>`__.
At times however the `stable kernel fix propagation mechanism
<http://wireless.kernel.org/en/users/Documentation/Fix_Propagation>`__
may not allow OEMs/OSVs/users to get the latest stable patches in time
for their own needs. Other times, some patches which are not necessarily
*"stable"* fixes may be desired to be integrated into a stable release
by a hardware manufacturer, for one reason or another. At times, a
subsystem maintainer may be on vacation and stable fix patches may take
a while to get to users. In the worst case scenario some patches may not
have been submitted for public review yet, for whatever reason, but it
may be desired to merge some temporary patches on a stable release.
Because of all these reasons compat-drivers has extended the `stable
kernel fix propagation mechanism
<http://wireless.kernel.org/en/users/Documentation/Fix_Propagation>`__
by enabling additional types of patches to be merged into a stable
release. **Upstream first** is still prioritized by categorizing these
patches.

There are five categories to compat-wireless' extra patches:

- pending-stable
- linux-next-cherry-picks
- linux-next-pending
- crap
- unified-drivers

We document these below.

Legend of additional patches
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Releases can either be vanilla releases or have a series of additional
patches applied to the release on top of the vanilla release. A flag is
appeneded at the end of a release to annotate patches from a specific
directory have been applied to this release. The appended extra flag
meanings follows::

    * -s - get and apply pending-stable/ from linux-next.git
    * -n - apply the patches linux-next-cherry-picks/ directory
    * -p - apply the patches on the linux-next-pending/ directory
    * -c - apply the patches on the crap/ directory
    * -u - apply the patches on the unified-drivers/ directory

Release with no extra flags are simply vanilla releases of the kernel.
Users / Linux distributions are encouraged to use the *-uspn* releases
if they are available, otherwise the *-spn* relases as these releases
will have extra fixes not yet propagated. The *-s* flag for example
indicates that the release has patches marked as stable which will be
released by the next 3.x.y release of the kernel so you might as well
get them now. Linux distributions are encouraged to use the extra
flagged releases as well. We provide the vanilla releases for those
Linux distributions which just want vanilla for whatever reason.

Each category is documented below.

Annotating cherry pick preference
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you want to annotate that a patch should be considered for inclusion
into the stable releases of compat-drivers under
linux-next-cherry-picks/ you can annotate it by adding a tag on the
commit log line, *Cc: backports@vger.kernel.org*. An example follows. By
annotating these patch a script can be used to scrape linux-next for
extracting these patches when building a stable release::

   commit 061acaae76dfb760f4f3fddf0cde43915b7d673c
   Author: Luis R. Rodriguez <rodrigue@qca.qualcomm.com>
   Date:   Wed Dec 7 21:50:07 2011 +0530

       cfg80211: allow following country IE power for custom regdom cards
       
       By definition WIPHY_FLAG_STRICT_REGULATORY was intended to allow the
       wiphy to adjust itself to the country IE power information if the
       card had no regulatory data but we had no way to tell cfg80211 that if
       the card also had its own custom regulatory domain (these are typically
       custom world regulatory domains) that we want to follow the country IE's
       noted values for power for each channel. We add support for this and
       document it.
       
       This is not a critical fix but a performance optimization for cards
       with custom regulatory domains that associate to an AP with sends
       out country IEs with a higher EIRP than the one on the custom
       regulatory domain. In practice the only driver affected right now
       are the Atheros drivers as they are the only drivers using both
       WIPHY_FLAG_STRICT_REGULATORY and WIPHY_FLAG_CUSTOM_REGULATORY --
       used on cards that have an Atheros world regulatory domain. Cards
       that have been programmed to follow a country specifically will not
       follow the country IE power. So although not a stable fix distributions
       should consider cherry picking this.
       
       Cc: backports@vger.kernel.org
       Cc: Paul Stewart <pstew@google.com>
       Cc: Rajkumar Manoharan <rmanohar@qca.qualcomm.com>
       Cc: Senthilkumar Balasubramanian <senthilb@qca.qualcomm.com>
       Reported-by: Rajkumar Manoharan <rmanohar@qca.qualcomm.com>
       Signed-off-by: Luis R. Rodriguez <mcgrof@qca.qualcomm.com>
       Signed-off-by: John W. Linville <linville@tuxdriver.com>

pending-stable patches
~~~~~~~~~~~~~~~~~~~~~~

These are patches merged into linux-next.git but that have not yet been
merged into the linux-stable.git tree.

From the pending-stable/README::

   Often right before the merge window we get a block on non
   oops/regression fixes for stable fixes. Some stable fixes
   often get propagated afterwards during the extraversion
   maintenance of the kernels. Right before the merge window
   circa rc4 and rc5 subsystem maintainers get pegged if they
   throw in non oops/regression fixes for Linus or their
   respective upstream maintainer. While this makes sense
   for tree management and stable release considerations we
   still need to get users some stable patches propagated.

   This directory is there to help with that. Only patches
   which have been merged into linux-next.git will be included
   in this directory which means you must post it and the maintainer
   should have merged it and Stephen would have picked it up.

   This directory will always be empty for bleeding edge
   releases as bleeding edge releases are always based on
   linux-next already. This directory only makes sense for
   stable release of the kernel, and it we will always try
   to use it, in case there are stable fixes not yet propagated.

linux-next-cherry-picks patches
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

From the linux-next-cherry-picks/README::

   We work hard to get patches in time onto the stable
   tree but sometimes a few things slip out, and sometimes a
   stable fix is simply too big in size to be merged into
   stable. In such cases though we do believe some of these
   patches are still relatively important to either enable new
   hardware which escaped the late rc cycles or to correct some
   behaviour which might be too late for stable. We apply
   these patches by default as they will be supported on these
   releases.

   The larger the number of patches you see in this directory
   the more we should be ashamed. We should strive to reduce this
   to 0 all the time.

   This directory will always be empty for bleeding edge
   releases as bleeding edge releases are always based on
   linux-next already.

linux-next-pending patches
~~~~~~~~~~~~~~~~~~~~~~~~~~

From the linux-next-pending/README::

   You must have a really good reason to be adding files
   in this directory. The requirement is your patches must have
   been posted to a public mailing list for the subsystem you are
   working on. Each patch you add here must have a really good
   explanation on the top of the file which clarifies why the
   patch has not yet been merged OR specify it on the commit log
   when you add it on compat-wireless.

   We try to avoid having patch files because but we understand
   if you might because you need to test code posted but not yet
   merged into linux-next or a stable kernel release. This can happen
   often during the merge window, when the maintainers are unavailable,
   on vacation, suck at what they do, or for any other uncontrollable
   reasons.

crap patches
~~~~~~~~~~~~

From the crap/README::

   If you are including patches into this directory you
   must be fixing some critical bug for a customer which needs
   immediate release or immediate testing.

   Alternatively you would use this to apply some sort of
   crap code you are maintaining.

   You must have a really good reason to be adding files
   in this directory. If possible you should explain your
   reasoning of why the patch is getting included here and
   not upstream and why it hasn't even yet been posted.

   You should avoid these patches at all costs.

unified-drivers patches
~~~~~~~~~~~~~~~~~~~~~~~

From the unified-drivers/README.md::

   compat-drivers supports developers to supply a unified
   driver git tree which is being used to target support
   for getting the driver in line with requirements for
   linux-next. Once the driver gets upstream the driver
   gets removed and we cherry pick updated version of the
   driver directly from linux upstream.

   The code provided on this tree must try to adhere to
   conventions for targetting inclusion into linux-next.
   The compat-drivers patches/unified-drivers/ directory
   allows for any additional required backport delta to
   be addressed for the supplied driver. This allows
   development and transformation of the driver to always
   be targetting linux-next and allows for backporting
   to be dealt with separately.
