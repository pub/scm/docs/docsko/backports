Documentation/integration
=========================

The backports project first started out by supporting making releases as
`packaged tarballs <Documentation/packaging>`__ whereby users can
compile future versions of device drivers on older versions of Linux. As
of tag backports-20141114 (first release branch will be based on
origin/linux-3.19.y) backports supports **kernel integration** support.

Kernel integration support allows you to:

- **atomically integrate** future versions of device drivers
- apply required patches to backport them
- add hooks into kconfig to make them visible and usable
- deal with conflicts with older versions of these same drivers

By supporting kernel integration, the backports project can now support
enabling use of automatically backported subsystems and drivers on older
kernels without requiring modules, everything can be **built-in**.

Kernel integration summary
--------------------------

In order to understand how kernel integration works familiarize yourself
first :doc:`the backports development flow <backports/hacking>` and
:doc:`the backports git tags and branches <backports/hacking>`.
Usage of integration involves a *kernel source* and *kernel target*. As
with :doc:`backports packaging <packaging>` each tag has a
respective upstream linux-next or linux stable respective tag which can
be used as a *kernel source*. The *kernel target* is a kernel tree of an
older Linux release. For example if you want to integrate linux-next tag
next-20141114 onto v3.15 you may want will want to use the tag
backports-20141114 on the backports tree; the *kernel source* will be
the linux-next tree tag next-20141114 (you can either reset the tree to
that tag or use --git-revision tag to ./gentree.py) and the *kernel
target* can be a linux git tree reset to the v3.15 tag. Each backports
tag should be usable on all older supported kernels, this means you
could have used as a *kernel target* a tree set to v3.0, v3.1.. up to
today's latest release.

Kernel integration use case
---------------------------

**Kernel integration** is an advanced technique part of the backports
project which should make automate the task of completely integrating a
specific version of a device driver based on future / current upstream
releases on older kernel trees. This is typically more important where
products are being supported using old kernels but the required device
driver is only available on a future version of Linux. Linux
distributions which want to make releases based on slightly older
version of Linux but need newer device drivers can also make use of
this. Kernel integration support enables two new important features when
using backports now:

- built-in support (non modular)
- support for backporting features / components which the kernel only
  supported via built-in

*How you use integration is up to you*, we provide tools to help with
the process and at least for now **make the first integration
seamless**. If you are resetting your tree every time you base your
release on a new version of Linux you can use kernel integration to bump
the supported device drivers up to the latest known stable release for
example, thereby minimizing the amount of manual cherry picks required.
The backports project only provides patches required to backport device
drivers from a future version of Linux down to a specific release of
Linux, although it does not maintain specific cherry picks outside of
what is required to get the driver to work on older versions of Linux
specific cherry picks can easily be aggregated on top of the carried set
of patches. Use of backports for cherry picking it outside of the scope
of support for the project but it simply consists of adding your patches
onto the patches/ directory, either in legacy patch form or SmPL form,
if this strategy is followed its recommended a new directory be used to
track these patches and a way to know if they need to be carried over
when moving to a new version of Linux / backports.

Integrating a release
---------------------

The same script that is used for generating :doc:`backports packaged
tarballs <packaging>` is used for kernel integration, gentree.py as
there is a lot of code to share between both strategies. An example will
be provided of how to integrate a future set of supported device drivers
onto an older release tree of the Linux kernel. Any kernel tree >= 3.0
can be used as a *kernel target*, the *kernel source* will be any tag
after next-20141114 from linux-next and backports-20141114, as well as
any Linux release after v3.19 using the respective backports stable
branch for that release. The example we provide here integrates
linux-next next-20141114 onto the Linux kernel tree reset to v3.15.

To backport linux-next tag next-20141114 onto a Linux tree reset to
v3.15 do, you must first ensure your backports tree is on
backports-20141114 and then you can use::

    $./gentree.py --integrate --clean --gitdebug --git-revision next-20141114 ~/linux-next/ ~/linux/

This assumes that ~/linux/ is a git tree reset to the tag v3.15. This
should take 1-2 minutes on a typical laptop / system.

Kernel integration menuconfig
-----------------------------

After kernel integration you will see an entry like the one that follows
on your 'make menuconfig' menu::

    [*] Backport Linux next-20141114-0-gd7e5a72 (backports backports-20141114-0-gded96b2)  --->

.. image:: ../media/integration-menuconfig-start-3.15.png

Under that menu you will see device drivers taken from linux-next tag
next-20141114 and backported for you for use on v3.15. Enabling one
device driver from a future version of Linux will require using only the
latest version of the subsystem modules and other subsystem drivers. For
example enabling cfg80211 and mac80211 from a future version of Linux
will require you to only use future version of the respective device
drivers. In order to enable the backported version of 802.11 drivers for
example, you will have to enable first::

       Networking support -->
         Wireless -->

But under that menu disable all options. You will also need to now
disable all 802.11 device drivers under (including CONFIG_WLAN)::

   Device Drivers  ---> 
     Network device support  ---> 
       [ ]   Wireless LAN  ---->

Now to enable 802.11 drivers jump to the backports submenu to now enable
backported 802.11 drivers::

       Backports -->
         cfg80211
         mac80211
         Wireless LAN  --->
           etc

You should now be able to enable any backports 802.11 device driver.
Here's an example of full list of things you should see as of
backports-20141114::

.. figure:: Integration-menuconfig-drivers-3.15.png

Audit kernel integration work
-----------------------------

Backports takes advantage of the previously supported --gitdebug flag
for backport packaging in order to allow atomically tracking each atomic
operation done when integrating all pieces of software from a future
kernel to be supported on the *target kernel*. For this particular
integration, for example, it generates 92 commits. Ordered with the last
commit first, these are::

   $ git log v3.15~1.. --pretty=oneline
   c066a39ad047947dafbc4418ff10dadd3ad07921 apply integration patch 001-enable-backports/0001-enable-backports-built-in.patch
   000658cb018b781a08f174df3b5521b4385de759 hooked backport to top level Kconfig
   d78c278758a81a16d5f7d3dfd0782600cf5376ed disable unsatisfied Makefile parts
   e9d0f1fbfa6ce67feb1cff978bca40875277c47f add kernel version dependencies
   f5b738e65019fd51201688e3080d71c9ad5e8477 disable impossible kconfig symbols
   7de02203cd5a501bd69ea6c98eac126f1eb57b61 rename config symbol / srctree usage
   f70c7f185ca87d2f52ad72732fda3f1fa70f0891 add (useful) defconfig files
   697ece5caa7e38f3ef34811b4440637c6d8fa080 convert select to depends on
   9bf64ce6127ef2abfa2463b1063a236863e14f7b adjust backports config symbols we port
   b42e104c94ab0d30967dc5a31062335370b10a4f prune Kconfig tree
   b52feb5fef8a887170eee04383e1986a153dccd9 verify sources on top level backports Kconfig
   61d328ea565d3f03573b3a99dd3f3896efef727b generate kernel version requirement Kconfig file
   900a22cca078a7a5793d7c3b2bb444066677539a apply backport SmPL patch collateral-evolutions/network/0031-sk_data_ready.cocci
   ef6d48763eb52f65bcd0eabfd8ae9e9f16e5074f apply backport SmPL patch collateral-evolutions/network/0019-usb_driver_lpm.cocci
   f14786714aff5c94ae49b6f0acc3af095f5fbf5f apply backport SmPL patch collateral-evolutions/network/0001-netlink-portid.cocci
   9f88ec9628756758132fe7c189c82575127ed838 apply backport SmPL patch collateral-evolutions/generic/0001-group-attr/0002-group_attr_bus.cocci
   93b0c05507001c338a4efc4d9f3699bfee286b85 apply backport SmPL patch collateral-evolutions/generic/0001-group-attr/0001-group_attr_class.cocci
   be4fb7cdd961f3d336086253712381190d7f8316 apply backport patch collateral-evolutions/network/0049-no-pfmemalloc/igb.patch
   d048c6abbaa4a17eeed64b75bf071dd83e575959 apply backport patch collateral-evolutions/network/0048-no_ndo_xmit_flush/igb.patch
   c606a1727efa4fb0aaddb103fc0ace9e7340110d apply backport patch collateral-evolutions/network/0047-write_iter/bluetooth.patch
   09a99346f1625aa0ce203de1134145be750d6dc7 apply backport patch collateral-evolutions/network/0046-mmc_rescan_entered/mwifiex.patch
   9fb5b3de864d7fa049ff0e304e2c04931351adbd apply backport patch collateral-evolutions/network/0044-no_user_ns_in_net/ieee802154.patch
   a79ce2806e10dc7ac13c0f0f00b43e08454a8c29 apply backport patch collateral-evolutions/network/0043-ndo_set_vf_rate/igb.patch
   01de0cf6c40dae21404e5fcf0f0e622a7cc4364c apply backport patch collateral-evolutions/network/0042-pci_error_handlers/ngene.patch
   f6a578dd6050dfb76b0c9e79b6e7e9fc1539697f apply backport patch collateral-evolutions/network/0042-pci_error_handlers/igb_pci_error_handlers.patch
   69c972277125ce73ca22d5436311c5718449669d apply backport patch collateral-evolutions/network/0042-pci_error_handlers/atl1e.patch
   87664cd587c14623c386ac583f7dfebedfd07469 apply backport patch collateral-evolutions/network/0042-pci_error_handlers/atl1c.patch
   102f85e8d0b698528bd05adb1fad73004330ca9e apply backport patch collateral-evolutions/network/0042-pci_error_handlers/alx.patch
   807d99b29a7186a398a3895df8c22911a46c5686 apply backport patch collateral-evolutions/network/0041-devm_hwmon_device_register_with_groups/igb-devm_hwmon_device_register_with_groups.patch
   5e399498c589af4f144a78590d6303bada824183 apply backport patch collateral-evolutions/network/0040-ethtool_cmd_mdix/igb_ethtool_cmd_mdix.patch
   041bb3abc060f04b0924e2c313479430257a0b8a apply backport patch collateral-evolutions/network/0039-ethtool_rxnfc/igb_rxnfc.patch
   91bc49e12a2c302eb6e264eeb6944806caaa2714 apply backport patch collateral-evolutions/network/0038-ethtool_rxfh_indir/igb_rxfh_indir.patch
   24fe54661213f4d82642dd7eaeae92314ee2ca55 apply backport patch collateral-evolutions/network/0037-ethtool_get_module/igb_get_module.patch
   5a8c3bf1e10a9dcd31f50c3acd3a42d6a4a5b3ac apply backport patch collateral-evolutions/network/0036-ethtool_eee/igb_ethtool_eee.patch
   237294691d58ec7ff6e41fc6f48587bd7d6c1f7c apply backport patch collateral-evolutions/network/0035-skb_no_fcs/igb_skb_no_fcs.patch
   fb1cb2e44c89847b04e2d021328ea548cbe05fa6 apply backport patch collateral-evolutions/network/0034-ndo_set_vf_spoofchk/igb_ndo_set_vf_spoofchk.patch
   f24bb572ee690b3aac0e89fa2d04888e8a036d7c apply backport patch collateral-evolutions/network/0033-ndo_vlan_rx_vid/igb_ndo_vlan_rx_vid.patch
   3878c64fc254f7acbd86e3539bf330b04730abb8 apply backport patch collateral-evolutions/network/0033-ndo_vlan_rx_vid/cdc_mbim.patch
   c888e6433a8b2ac7c3e0058a4936836ab15c3f6d apply backport patch collateral-evolutions/network/0032-sriov_configure/igb_sriov_configure.patch
   59ac89cb5e1fa8876555639bab09bc4a97673db5 apply backport patch collateral-evolutions/network/0030-qdisc_tx_busylock/ieee802154.patch
   506736ea6f59fed1d323f77cf03915ebc6df3358 apply backport patch collateral-evolutions/network/0030-qdisc_tx_busylock/bluetooth.patch
   5131db4cbec4ac8d7a6fb69246910644e0a8087d apply backport patch collateral-evolutions/network/0029-struct_hid_ll_driver-extensions/net_bluetooth_hidp_core.patch
   9575c214edcc921e75910b881d3db6a69cd4b577 apply backport patch collateral-evolutions/network/0028-select_queue/mwifiex.patch
   eafd9157010633a1cc7b025382b72f9bea8bfe8b apply backport patch collateral-evolutions/network/0028-select_queue/mac80211.patch
   4b7d5be3639ec2178a398c6a0730a8a9098f067e apply backport patch collateral-evolutions/network/0027-genl-const/nl80211.patch
   2283432684181d75e6288b2bd4d9f502c713087d apply backport patch collateral-evolutions/network/0027-genl-const/nfc.patch
   e5fec49c6b5c59dffe06f11a099e8df2c38de2cd apply backport patch collateral-evolutions/network/0027-genl-const/ieee802154.patch
   7b0bd1c47c0a22240593c3860ea78e87310531db apply backport patch collateral-evolutions/network/0027-genl-const/hwsim.patch
   3156a48fc2aa1eb028d5074d0f15f02a2a784366 apply backport patch collateral-evolutions/network/0026-ipv6_stub/cdc_mbim.patch
   ab5ca0e7db55e7a68bae27c278dc159be531caf7 apply backport patch collateral-evolutions/network/0025-usb-sg/usbnet.patch
   18634861a944029a6623f5dc582482d7fdd87bba apply backport patch collateral-evolutions/network/0024-led-blink-api/mac80211.patch
   6895c3c9fefe90302577ccc0207e574dab5db4fb apply backport patch collateral-evolutions/network/0022-define-tracing/wil6210.patch
   61b12eb789add8ea4f42343ba5754a2003f9ff85 apply backport patch collateral-evolutions/network/0022-define-tracing/cfg80211.patch
   4d362e9144e6189248976e83b797e93b5be2210a apply backport patch collateral-evolutions/network/0022-define-tracing/ath6kl.patch
   a7a3cebf434a294ef089823eedf7c2db8b41a042 apply backport patch collateral-evolutions/network/0021-umode_t-api-change/ath10k.patch
   a25e701fe08b1f124307c17cfa1eb1879b26693d apply backport patch collateral-evolutions/network/0020-tty-termios/net_bluetooth_rfcomm_tty.patch
   829c2477a533e03c8df4f64d60d3acbc260f2090 apply backport patch collateral-evolutions/network/0020-tty-termios/drivers_bluetooth_hci_ath.patch
   4793ad2d27a5a98b99f1af0cc4369f4d2dc67a78 apply backport patch collateral-evolutions/network/0018-pv-trace-fixes/net_mac80211_trace.patch
   cc3fcdfcd106be170937111f5b1e9282ea8b2f43 apply backport patch collateral-evolutions/network/0018-pv-trace-fixes/drivers_net_wireless_iwlwifi_iwl-debug.patch
   77b4d3a92296542ada9af173729c29ff8edaa4f0 apply backport patch collateral-evolutions/network/0017-get_ts_info/igb_get_ts_info.patch
   3a1b78fee0ed903f167f2038ce3f7b34b80fff31 apply backport patch collateral-evolutions/network/0017-get_ts_info/drivers_net_usb_usbnet.patch
   7af04292e12cd6a3bd4933cbc2db33f678935985 apply backport patch collateral-evolutions/network/0017-get_ts_info/cdc_ncm.patch
   c9c75a56434fa88c3b5387964d390c957109bb0e apply backport patch collateral-evolutions/network/0016-libertas-olpc-ec-wakeup/drivers_net_wireless_libertas_if_usb.patch
   63959daeafbac9ff38dbc847cbbb9a61501be851 apply backport patch collateral-evolutions/network/0015-rename_pm_qos_request/drivers_net_wireless_ipw2x00_ipw2100.patch
   5e7f8f9b32dcb984445bde43f69656c97b31c7e7 apply backport patch collateral-evolutions/network/0014-netlink_seq/net_wireless_nl80211.patch
   35d5785bfdcdf579493f3076f3ac9bc397ce9597 apply backport patch collateral-evolutions/network/0013-fix-makefile-includes/net_wireless_Makefile.patch
   5adbb25691d4745f5f47ffc493bfd32a063480ca apply backport patch collateral-evolutions/network/0013-fix-makefile-includes/bluetooth.patch
   2258d123aec87c0a11515130d9297587633fdafc apply backport patch collateral-evolutions/network/0012-driver-quirks/drivers_net_wireless_cw1200_sdio.patch
   d1f6fc792d3fba9ee20f31562c04e34af0f6daab apply backport patch collateral-evolutions/network/0011-mac80211-disable-tx-status/net_mac80211_tx.patch
   a146dcd4c0db925dc8a3fec1ee93074b0a192c67 apply backport patch collateral-evolutions/network/0010-add-wext-handlers-to-netdev/net_wireless_core.patch
   5e1b6f56f043608ee68cef35d7deda383b78fd01 apply backport patch collateral-evolutions/network/0008-net-user-ns.patch
   5000b38d3dcc9434b19eaccde8aca21f0b271e12 apply backport patch collateral-evolutions/network/0007-lowpan-inet_frag_lru_move.patch
   2ae7bbbdb7eb2c02058327dfc057ad841aede913 apply backport patch collateral-evolutions/network/0006-ieee802154-6lowpan-namespace.patch
   fc28048373ae3c6637cedab606919e7817f6ffad apply backport patch collateral-evolutions/network/0005-disable-usb-net-asix.patch
   50e3e5f2151ed76aef9a1ac7352959a806e55e6f apply backport patch collateral-evolutions/network/0004-disable-wext-kconfig.patch
   98b551a9316154702270ac7164916e49180c0046 apply backport patch collateral-evolutions/network/0003-cfg80211-wext-padding/net_wireless_core.patch
   0f5f1ae4483b16f187f074b676d19212dab814f8 apply backport patch collateral-evolutions/network/0003-cfg80211-wext-padding/include_net_cfg80211.patch
   8cfeb3d0bdec62cccb0cf76f44ca2e7e5c5e9ff0 apply backport patch collateral-evolutions/network/0002-disable-dump-adjust-on-old-kernels.patch
   166003e19c7a9ff28a43004ad27d68af3a3aace0 apply backport patch collateral-evolutions/media/0004-missing-include/ir-hix5hd2.patch
   f1152ca58d2400a956a969ae88b80c35c449d6dc apply backport patch collateral-evolutions/media/0004-missing-include/hackrf.patch
   94f80b0f595162569b70684d7ed87612b31a764f apply backport patch collateral-evolutions/media/0004-missing-include/airspy.patch
   6d974050060a1d74d61b1927ac0f18ca0267e3d5 apply backport patch collateral-evolutions/media/0003-devnode_param_type/rc.patch
   2678364bc96c15c9e39bdf901f60fb8bef5d9851 apply backport patch collateral-evolutions/media/0003-devnode_param_type/dvb-core.patch
   04361b3134ed2cd078920eb651ec1a15c038a307 apply backport patch collateral-evolutions/media/0003-devnode_param_type/ddbridge.patch
   b0a68f9e7b235ad44f64686a7657d65ededaeb87 apply backport patch collateral-evolutions/media/0002-no_dmabuf/v4l2.patch
   102162ef4bda228fc2de08826f8c304001503b67 apply backport patch collateral-evolutions/media/0001-dma_mmap_coherent-revert.patch
   780d84bd410b4252cfc4df431b5c0e8c6133fcef apply backport patch 0000-upstream-backport-changes/0005-uapi-compromise/mwifiex.patch
   6a77c4dc3c16907373648a774678c0bf63b1f786 Add automatic backports
   6e8820dfdd11eb2ef361f344ece8170e6776a151 modify top level backports/Kconfig with backports identity
   c1e39709cfde12068fa3b693d71f72fc536d4df4 verify sources for automatic backports
   9b34df472df8dbf0c3b518eab7a5e378c966fa3f Add driver sources
   5b8285fa632ad085753da8e930c0ec2275c191ff Copied backport
   1860e379875dfe7271c649058aeddffe5afd9d0d Linux 3.15
