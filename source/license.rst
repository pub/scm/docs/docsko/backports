License
=======

Linux kernel backports license
------------------------------

This work is a subset of the Linux kernel as such we keep the kernel's
Copyright practice. Some files have their own copyright and in those
cases the license is mentioned in the file. All additional work made to
building this package is licensed under the GPLv2.

Backporting preference for EXPORT_SYMBOL_GPL() and proprietary drivers
----------------------------------------------------------------------

The Linux kernel backports framework was designed by intent since its
inception \*only\* for upstream Linux kernel drivers to avoid excuses
about the difficulty to support upstream Linux kernel development while
also supporting users on older kernels. To ensure the intent is
respected currently all symbols that we do work on to backport are
exported via EXPORT_SYMBOL_GPL() as we do work to backport them for the
supported kernels. By using EXPORT_SYMBOL_GPL() we make it \*clear\*
that if you use the backported symbols your software \*is\* considered
derivative works of the Linux kernel.

Developer's Certificate of Origin 1.1
-------------------------------------

Linux backports relies on the 'Developer's Certificate of Origin 1.1',
for more details refer to :doc:`the submitting patches documentation
<documentation/backports/hacking>`.
