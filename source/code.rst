Code
====

In order to backport the Linux kernel we must divide and conquer the
tasks at hand.

This is now deprecated use :doc:`hacking on backports <documentation/backports/hacking>`.

Backport components
-------------------

There are currently two components to the backports effort.

compat
~~~~~~

compat is a shared module / headers that implements newer features for usage on older kernels.

- git://github.com/mcgrof/compat.git>`__
- :doc:`documentation/compat`

compat-drivers
~~~~~~~~~~~~~~

compat-drivers is the framework that pulls code from the Linux kernel,
adds the compat module and backports features that compat could not
backport within its module / headers.

- git://github.com/mcgrof/compat-drivers.git``
- :doc:`documentation/compat-drivers`

Getting all required code
-------------------------

To be able to make releases and test building releases however you need
quite a bit more code. The setup is simplified with scripts from the
compat.git tree. To set your system up for the first time you can run::

   wget https://github.com/mcgrof/compat/raw/master/bin/get-compat-trees
   wget https://github.com/mcgrof/compat/raw/master/bin/get-compat-kernels

   chmod 755 get-compat-trees get-compat-kernels 

   ./get-compat-trees
   ./get-compat-kernels

Hacking on the project
----------------------

You should realize that there are a few components to the project and
that although they are used together each one can be considered
independent of each other.

- Read the :doc:`hacking on compat documentation <documentation/compat>`
- Read the :doc:`hacking on compat-drivers documentation <documentation/compat-drivers>`

TODO list
~~~~~~~~~

Read the :doc:`hacking TODO <documentation/compat-drivers/hacking>` for
a list of wish items we should be working towards.
