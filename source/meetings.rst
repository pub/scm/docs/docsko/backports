Meetings
========

Backport project meetings
-------------------------

At times we may meet. This page is dedicated towards documenting any
meetings that have taken place or will take place.

Linux Plumbers 2013 - New Orleans
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There will be a talk about backporting at LPC 2013.

Sept 4 2012 meeting
~~~~~~~~~~~~~~~~~~~

On Sept 4, 2012 we had our first `G+ Linux kernel backports hangout
<https://plus.google.com/events/cbsid8cbvp61sv4nmjfgftsv9f0/101487746899672598413>`__
at. This took place at:

- 08:00:00 p.m. Tuesday September 4, 2012 in GMT
- 01:00:00 p.m. Tuesday September 4, 2012 in US/Pacific
- 04:00:00 p.m. Tuesday September 4, 2012 in US/Eastern
- 10:00:00 p.m. Tuesday September 4, 2012 in Europe/Berlin
- 11:00:00 p.m. Tuesday September 4, 2012 in Europe/Helsinki
- 11:00:00 p.m. Tuesday September 4, 2012 in Asia/Jerusalem
- 01:30:00 a.m. Wednesday September 5, 2012 in Asia/Calcutta

Recording of the session:

http://www.youtube.com/watch?v=FZqDz7sXZiQ

Agenda:

- Current project objectives:

    - 802.11, Bluetooth, Ethernet, drm drivers
    - Vanilla kernels are supported but also now:

        - RHEL6

    - Coccinelle SmPL - 0001-netdev_ops.patch -- collateral evolution work
    - Build server for helping with testing patches
    - git tree mirroring on git.kernel.org
    - Daily linux-next based snapshots via kernel.org
    - Public facing releases via kernel.org as of v3.7-rc1

- Things to look out for:

    - linux-firmware.git updates - script install target ?
    - Module key signing & UEFI - Fedora 18 Alpha release schedule for 2012-09-11
    - Understanding the implications of a stable extra version release update: ee9c8a04 backported to v3.4.6
